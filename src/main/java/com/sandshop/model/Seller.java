package com.sandshop.model;

import com.sandshop.model.base.User;

import java.util.ArrayList;
import java.util.List;

public class Seller extends User {

    private List<ProductItem> items;

    public Seller(String name, String address) {
        super(name, address);
        items = new ArrayList<ProductItem>();
    }

    public List<ProductItem> getItems(){
        return items;
    }

}
