package com.sandshop.model;

import java.util.*;

public class Order {
    private int orderId;
    private Map<ProductItem, Integer> items;
    //private List<ProductItem> items;
    private Buyer buyer;
    private OrderStatus orderStatus;
    private Date orderDate;
    private int itemsPrice;

    public Order(Buyer buyer) {
        this.buyer = buyer;
        items = new HashMap<ProductItem, Integer>();
        orderStatus = OrderStatus.INITIAL;
        orderId = new Random().nextInt();
        orderDate = new Date();

        itemsPrice = getSum();
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getItemsPrice() {
        return itemsPrice;
    }

    public Map<ProductItem, Integer> getItems() {
        return items;
    }

    public void addItem(ProductItem item, Integer amount) {
        items.put(item, amount);
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    private int getSum() {
        int sum = 0;
        for (Map.Entry<ProductItem, Integer> entry : items.entrySet()) {
            sum += entry.getKey().getPrice() * entry.getValue();
        }
        return sum;
    }

    @Override
    public String toString() {
        return orderId + " " + items + " " + buyer + " " + orderStatus + " " + orderDate + " " + itemsPrice;
    }
}
