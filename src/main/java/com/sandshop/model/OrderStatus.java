package com.sandshop.model;

public enum OrderStatus {
    INITIAL,
    PROCESSING,
    CANCELED,
    FINISHED

}
