package com.sandshop.model;

public class ProductItem {
    private String name;
    private int price;
    private boolean isAvailable;

    public ProductItem(String name, int price, boolean isAvailable) {
        this.name = name;
        this.price = price;
        this.isAvailable = isAvailable;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductItem that = (ProductItem) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();

    }

    @Override
    public String toString() {
        return name + " " + price + " " + isAvailable;
    }
}
