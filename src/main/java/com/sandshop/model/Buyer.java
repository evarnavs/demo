package com.sandshop.model;

import com.sandshop.model.base.User;

import java.util.ArrayList;
import java.util.List;

public class Buyer extends User {
    private int moneyLimit;
    List<Order> orders;

    public List<Order> getOrders() {
        return orders;
    }

    public void addOrder(Order order) {
        orders.add(order);

    }

    public Buyer(String name, String address, int moneyLimit) {
        super(name, address);
        this.moneyLimit = moneyLimit;
        orders = new ArrayList<Order>();
    }

    public int getMoneyLimit() {
        return moneyLimit;
    }
}
