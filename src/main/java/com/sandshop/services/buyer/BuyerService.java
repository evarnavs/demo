package com.sandshop.services.buyer;

import com.sandshop.model.Order;
import com.sandshop.model.OrderStatus;
import com.sandshop.model.Buyer;

import java.util.ArrayList;
import java.util.List;

public class BuyerService implements IBuyerService {
    public void sendOrder(Order order)  {
        //send order

        if(order.getItemsPrice() > order.getBuyer().getMoneyLimit()){
            throw new NotEnoughMoneyException();        }
    }

    public List<Order> getMyOrders(Buyer buyer){
        return new ArrayList<Order>();
    }

    public List<Order> getMyOrders(Buyer buyer, OrderStatus orderStatus){
        return new ArrayList<Order>();
    }
}
