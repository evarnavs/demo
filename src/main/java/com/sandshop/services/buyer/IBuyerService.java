package com.sandshop.services.buyer;

import com.sandshop.model.Order;
import com.sandshop.model.OrderStatus;
import com.sandshop.model.Buyer;

import java.util.List;

public interface IBuyerService {

    /**
     * Sends order
     * @param order
     * @throws NotEnoughMoneyException if not enough money buyer has
     */
    void sendOrder(Order order);

    List<Order> getMyOrders(Buyer buyer);
    List<Order> getMyOrders(Buyer buyer, OrderStatus orderStatus);

}
