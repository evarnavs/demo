package com.sandshop.services.seller;

import com.sandshop.model.Order;
import com.sandshop.model.OrderStatus;

public class SellerServiceImpl implements ISellerService {
    public void changeStatus(Order order, OrderStatus orderStatus) {
        order.setOrderStatus(orderStatus);
        System.out.println("order id = " + order.getOrderId() + "was changed" + orderStatus);
    }
}
