package com.sandshop.services.seller;

import com.sandshop.model.Order;
import com.sandshop.model.OrderStatus;

public interface ISellerService {
    void changeStatus(Order order, OrderStatus status);
}
