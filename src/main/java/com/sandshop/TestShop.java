package com.sandshop;

import com.sandshop.model.Buyer;
import com.sandshop.model.Order;
import com.sandshop.model.ProductItem;
import com.sandshop.repo.OrderRepo;
import com.sandshop.repo.ProductItemRepo;
import com.sandshop.repo.UserRepo;

public class TestShop {
    public static void main(String[] args) {
        ProductItemRepo pir = new ProductItemRepo();
        ProductItem good1 = new ProductItem("good1", 100,true);
        pir.addItem(good1);
        ProductItem good2 = new ProductItem("good2", 60,true);
        pir.addItem(good2);
        ProductItem good3 = new ProductItem("good3", 120,true);
        pir.addItem(good3);

        System.out.println(pir.getAll());
        Buyer ursa = new Buyer("Ursa", "Voronezh", 10000);
        UserRepo ur = new UserRepo();
        ur.addUser(ursa);

        OrderRepo or = new OrderRepo();
        Order order1 = new Order(ursa);
        order1.addItem(good1,2);
        or.addOrder(order1);
        Order order2 = new Order(ursa);
        or.addOrder(order2);

        System.out.println(or.getOrdersForUser(ursa.getName()));

    }
}
