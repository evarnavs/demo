
package com.sandshop.repo;

import com.sandshop.model.Order;
import com.sandshop.model.ProductItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OrderRepo {
    List<Order> orderList;

    public OrderRepo() {
        orderList = new ArrayList<Order>();
    }

    public void addOrder(Order order) {
        orderList.add(order);
    }

    public List<Order> getOrdersForUser(String userName) {
        List<Order> results = new ArrayList<Order>();
        for (Order order : orderList) {
            if (order.getBuyer().getName().equals(userName)) {
                results.add(order);
            }
        }
        return results;
    }

    //get orders Item name

    public List<Order> getOrdersForItem(String itemName) {
        List<Order> results = new ArrayList<Order>();
        for (Order order : orderList) {
            for (ProductItem item : order.getItems().keySet()) {

                if (item.getName().equals(itemName)) {
                    results.add(order);
                }
            }
        }
        return results;
    }

}
