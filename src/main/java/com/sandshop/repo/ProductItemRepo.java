package com.sandshop.repo;

import com.sandshop.model.ProductItem;

import java.util.ArrayList;
import java.util.List;

public class ProductItemRepo {
    List<ProductItem> itemList;
//    public List<ProductItem> getIetms(){
//        return Arrays.asList(new ProductItem("dogfood", 1000,true),
//        new ProductItem("catfood",200,true),
//        new ProductItem("fishfood",65,true));
//    }

    public ProductItemRepo() {
        itemList = new ArrayList<ProductItem>();
    }

    public void addItem(ProductItem item) {
        itemList.add(item);
    }

    public ProductItem getItem(String itemName) {
        for (ProductItem i : itemList) {
            if (i.getName().equals(itemName)) {
                return i;
            }
        }
        return null;

    }

    public List<ProductItem> getAll(){
        return itemList;
    }


}
