package com.sandshop.repo;

import com.sandshop.model.Buyer;
import com.sandshop.model.base.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepo {
    List<User> userRepo;

    public UserRepo(){
        userRepo = new ArrayList<User>();
    }

    public void addUser (User user){
        userRepo.add(user);
    }

    public User getUser (String name){
        for (User user : userRepo){
            if (user.getName().equals(name)){
                return user;
            }
        }
        return null;
    }
}
